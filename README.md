# CNS parser

A parser for Dati Personali contained in your Italian Carta Nazionale dei Servizi.

## Components
* lib: cnsparser's library source code
* test: testing program

## Compile
    $ cd $REPOSITORY
    $ mkdir build-$COMPONENT
    $ cd build-$COMPONENT
    $ qmake ../$COMPONENT
    $ make -j$(nproc)

## Install
First compile using the above commands, then:
    # make install
or, if you want to install to a custom root directory:
    $ make INSTALL_ROOT="/tmp" install

## Package
### ArchLinux
    $ cd $REPOSITORY/pkg/tar.xz
    $ makepkg

## Disable debugging
Populate the following environment variable:
    `QT_LOGGING_RULES="*.debug=false;"`
