#include "CNS.h"

/*
``
    pcsc_stringify_error()
        The returned string uses a Thread-Local Storage (TLS) buffer and is valid: [...]
            only while the thread on which it was obtained is alive.
''
 */
#define CHECK(what, rv) \
if (rv != SCARD_S_SUCCESS) { \
    char message[256]; \
    sprintf(message, what ": %s\n", pcsc_stringify_error(rv)); \
    throw ExPCSC(message); \
}

int read_dati_personali_c(char** output_buffer, int* len) {
    LONG rv;

    SCARDCONTEXT hContext;
    LPTSTR mszReaders;
    SCARDHANDLE hCard;
    DWORD dwReaders, dwActiveProtocol, dwRecvLength;

    SCARD_IO_REQUEST pioSendPci;
    BYTE pbRecvBuffer[1024];
    BYTE cmd[4][7];
    memcpy(cmd[0], "\x00\xA4\x00\x00\x02\03f\x00", 7);
    memcpy(cmd[1], "\x00\xA4\x00\x00\x02\x11\x00", 7);
    memcpy(cmd[2], "\x00\xA4\x00\x00\x02\x11\x02", 7);
    memcpy(cmd[3], "\x00\xB0\x00\x00\x00\x00", 6);

    rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext);
    CHECK("SCardEstablishContext", rv)

#ifdef SCARD_AUTOALLOCATE
    dwReaders = SCARD_AUTOALLOCATE;

    rv = SCardListReaders(hContext, NULL, (LPTSTR)&mszReaders, &dwReaders);
    CHECK("SCardListReaders", rv)
#else
    rv = SCardListReaders(hContext, NULL, NULL, &dwReaders);
    CHECK("SCardListReaders", rv)

    mszReaders = calloc(dwReaders, sizeof(char));
    rv = SCardListReaders(hContext, NULL, mszReaders, &dwReaders);
    CHECK("SCardListReaders", rv)
#endif
    /* printf("reader name: %s\n", mszReaders); */

    rv = SCardConnect(hContext, mszReaders, SCARD_SHARE_SHARED,
    SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard, &dwActiveProtocol);
    CHECK("SCardConnect", rv)

    switch(dwActiveProtocol) {
        case SCARD_PROTOCOL_T0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case SCARD_PROTOCOL_T1:
            pioSendPci = *SCARD_PCI_T1;
            break;
    }

    for (int j = 0; j < 4; ++j) {
        dwRecvLength = sizeof(pbRecvBuffer);
        /* TODO: hardcoded packet length*/
        rv = SCardTransmit(hCard, &pioSendPci, cmd[j], ((j == 3) ? 6 : 7), NULL, pbRecvBuffer, &dwRecvLength);
        CHECK("SCardTransmit", rv)
        /*
        printf("response %d\tlen %d\t", j, dwRecvLength);
        printf("%.*s", dwRecvLength, pbRecvBuffer);
        printf("\n");
        */
    }

    /* send back last buffer read */
    *output_buffer = (char*)malloc(dwRecvLength);
    memcpy(*output_buffer, pbRecvBuffer, dwRecvLength);
    *len = dwRecvLength;

    rv = SCardDisconnect(hCard, SCARD_LEAVE_CARD);
    CHECK("SCardDisconnect", rv)

#ifdef SCARD_AUTOALLOCATE
    rv = SCardFreeMemory(hContext, mszReaders);
    CHECK("SCardFreeMemory", rv)
#else
    free(mszReaders);
#endif

    rv = SCardReleaseContext(hContext);

    CHECK("SCardReleaseContext", rv)

    return 0;
}

