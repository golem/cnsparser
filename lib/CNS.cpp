#include "CNS.h"

void CNS::fromSmartCard(void) {
    char* buffer;
    int len;
    int rv = read_dati_personali_c(&buffer, &len);
    if (rv != 0) throw ExPCSC("can not read_dati_personali_c()");

    std::vector<std::string> field;

    for (int i = 12; i < len; ) {
        if (buffer[i] == '\0') break;

        std::string hexstring(&buffer[i], 2);
        int len = std::stoi(hexstring, nullptr, 16);
        i += 2;
        std::string fieldData(&buffer[i], len);
        i += len;

        field.push_back(fieldData);
    }
    free(buffer);

    for (int i = 0; i < (int)field.size(); ++i) {
        qDebug() << i << '\t' << field[i].data();
        switch (i) {
            case 0: this->dataRilascio = QDate::fromString(QString(field[i].data()), "ddMMyyyy"); break;
            case 1: this->dataScadenza = QDate::fromString(QString(field[i].data()), "ddMMyyyy"); break;
            case 2: this->cognome = QString(field[i].data()); break;
            case 3: this->nome = QString(field[i].data()); break;
            case 4: this->dataNascita = QDate::fromString(QString(field[i].data()), "ddMMyyyy"); break;
            case 5: this->sesso = field[i] == "F" ? F : M; break;  /* TODO what about other genders? */
            case 7: this->codiceFiscale = QString(field[i].data()); break;
            case 9: this->comuneNascita = QString(field[i].data()); break;
            case 12: this->comuneResidenza = QString(field[i].data()); break;
            default: break;
        }
    }

}

QTextStream& operator<< (QTextStream& os, const CNS& cns) {
    os << "DataRilascio:\t" << cns.dataRilascio.year() << '-' << cns.dataRilascio.month() << '-' << cns.dataRilascio.day() << endl;
    os << "DataScadenza:\t" << cns.dataScadenza.year() << '-' << cns.dataScadenza.month() << '-' << cns.dataScadenza.day() << endl;
    os << "Nome:\t" << cns.nome << endl;
    os << "Cognome:\t" << cns.cognome << endl;
    os << "DataNascita:\t" << cns.dataNascita.year() << '-' << cns.dataNascita.month() << '-' << cns.dataNascita.day() << endl;
    os << "ComuneNascita:\t" << cns.comuneNascita << endl;
    os << "ComuneResidenza:\t" << cns.comuneResidenza << endl;
    os << "CodiceFiscale:\t" << cns.codiceFiscale << endl;
    os << "Sesso:\t" << ((cns.sesso == F) ? 'F' : 'M') << endl;

    return os;
}

QString CNS::getNome() {
    return this->nome;
}

QString CNS::getCognome() {
    return this->cognome;
}

QDate CNS::getDataNascita() {
    return this->dataNascita;
}

QString CNS::getComuneNascita() {
    return this->comuneNascita;
}

QString CNS::getComuneResidenza() {
    return this->comuneResidenza;
}

QString CNS::getCodiceFiscale() {
    return this->codiceFiscale;
}

Sex CNS::getSesso() {
    return this->sesso;
}

QDate CNS::getDataRilascio() {
    return this->dataRilascio;
}

QDate CNS::getDataScadenza() {
    return this->dataScadenza;
}
