#include "CNS.h"

Ex::Ex() {
    this->msg = std::string("generic exception");
}

Ex::Ex(const char* msg) {
    this->msg = std::string(msg);
}

Ex::Ex(const std::string& msg) {
    this->msg = msg;
}

std::ostream& operator<<(std::ostream& os, const Ex& e) {
    os << e.msg;
    return os;
}
