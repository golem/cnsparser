#----------------------------------------------------
# CNS-Parser for Italian Carta Nazionale dei Servizi
# adapted by giomba
#----------------------------------------------------

QT       += core

TARGET = cnsparser
TEMPLATE = lib

DEFINES += QT_DEPRECATED_WARNINGS CNSPARSER_LIBRARY

CONFIG += c++11 link_pkgconfig
PKGCONFIG += libpcsclite

SOURCES += \
        CNS.cpp \
        Ex.cpp \
        read_dati_personali_c.cpp

HEADERS += \
        CNS.h

target.path = /usr/lib
headers.path = /usr/include
headers.files += $$HEADERS

INSTALLS += target headers
