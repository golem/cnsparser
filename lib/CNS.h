#ifndef CNS_H
#define CNS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winscard.h>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <QDate>
#include <QDebug>
#include <QString>
#include <QTextStream>

#ifdef CNSPARSER_LIBRARY
    #define CNSPARSER_EXPORT Q_DECL_EXPORT
#else
    #define CNSPARSER_EXPORT Q_DECL_IMPORT
#endif

// PCSCLITE
// --------------------------------------------------------------------
int read_dati_personali_c(char** output_buffer, int* len);

// EXCEPTIONS
// --------------------------------------------------------------------
/* `explicit` specifier forces constructors to not being implicitly called */
class Ex {
    private:
        std::string msg;
    public:
        explicit Ex();
        explicit Ex(const char* msg);
        explicit Ex(const std::string& msg);
        friend std::ostream& operator<<(std::ostream&, const Ex&);
};

/* Parent::Parent makes classes inherit their constructors from parents (C++11) */
class ExPCSC : public Ex { using Ex::Ex; };

// CNS
// --------------------------------------------------------------------
enum Sex {
    F,
    M
};

class CNSPARSER_EXPORT CNS {
    private:
        QString nome;
        QString cognome;
        QDate dataNascita;
        QString comuneNascita;
        QString comuneResidenza;
        QString codiceFiscale;
        Sex sesso;
        QDate dataRilascio;
        QDate dataScadenza;
    public:
        void fromSmartCard(void);
        friend QTextStream& operator<<(QTextStream& os, const CNS& cns);

        QString getNome();
        QString getCognome();
        QDate getDataNascita();
        QString getComuneNascita();
        QString getComuneResidenza();
        QString getCodiceFiscale();
        Sex getSesso();
        QDate getDataRilascio();
        QDate getDataScadenza();
};


#endif

