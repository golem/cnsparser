#!/bin/bash

SLASHES=$(echo $0 | tr -d -c '/'  | awk '{ print length($0) }')

if [ $SLASHES != 1 ]; then
    echo -e "run in my directory" >&2
    exit 1
fi

cd ../..

mkdir -p build-lib
cd build-lib

qmake ../lib

make -j $(nproc)

checkinstall --type debian \
    --install=no \
    --pkgname=cnsparser \
    --pkgversion=$(date +%Y.%m) \
    --pkgrelease=1 \
    --arch=x86_64 \
    --pkglicense=GPL \
    --maintainer=$(git config user.name)

