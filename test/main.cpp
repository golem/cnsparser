#include <iostream>

#include <QTextStream>

#include <CNS.h>

int main(void) {
    CNS cns;
    int r = 0;

    try {
        cns.fromSmartCard();

        QTextStream out(stdout);
        out << cns << endl;

        r = 0;
    }  catch (Ex e) {
        std::cerr << "[E] exception: " << e << std::endl;
        r = 1;
    }

    return r;
}
